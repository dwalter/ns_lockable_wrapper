/*
    ns_lockable_pointer_t.h : example source for non-stop wait-free/weight-less
    [ low fat ] locking using extra bits in 64bit pointer
      
    Copyright (C) 2012 David Walter<walter.david@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

 */

#ifndef NS_LOCKABLE_POINTER_T_H
#define NS_LOCKABLE_POINTER_T_H
#include <ns_lockable_wrapper/ns_lockable_t.h>
#ifndef NS_LOCKABLE_T_H
#error Do not #include <ns_lockable_wrapper/ns_lockable_pointer_t.h> without
#error #include <ns_lockable_wrapper/ns_lockable_t.h> without
#endif
#include <stdexcept>

namespace ns
{
  struct null_ptr_t : std::runtime_error
  {
    null_ptr_t( const char* text ): std::runtime_error( text ){}
    null_ptr_t( const std::string& text ): std::runtime_error( text ){}
  } ;
  struct null_lockable_t : std::runtime_error
  {
    null_lockable_t( const char* text ): std::runtime_error( text ){}
    null_lockable_t( const std::string& text ): std::runtime_error( text ){}
  } ;

  template <typename type_t>
  struct lockable_pointer_t
  {
    lockable_t* lockable ;
    lockable_pointer_t():
      lockable(0)
    {
    }

    ~lockable_pointer_t(){}

    lockable_pointer_t( lockable_pointer_t&& rhs ):
      lockable( rhs.lockable )
    {
      if ( lockable )
      {
	std::cerr
	  << __FILE__
	  << ":"
	  << __LINE__
	  << ":lockable->references["
	  << lockable->references
	  << "] *pointer()["
	  << *lockable
	  << "]"
	  << std::endl ;
      }
    }

    lockable_pointer_t( lockable_pointer_t& rhs ):
      lockable( rhs.lockable )
    {
      if ( lockable )
      {
	std::cerr
	  << __FILE__
	  << ":"
	  << __LINE__
	  << ":lockable->references["
	  << lockable->references
	  << "] *pointer()["
	  << *pointer()
	  << "]"
	  << std::endl ;
      }
    }

    lockable_pointer_t( lockable_t* rhs ):lockable( rhs )
    {
      if ( lockable )
      {
	std::cerr
	  << __FILE__
	  << ":"
	  << __LINE__
	  << ":lockable->references["
	  << lockable->references
	  << "] *pointer()["
	  << *pointer()
	  << "]"
	  << std::endl ;
      }
    }

    lockable_pointer_t( type_t* rhs ):
      lockable( new lockable_t( rhs ) )
    {
      if ( rhs )
      {
	std::cerr
	  << __FILE__
	  << ":"
	  << __LINE__
	  << ":lockable->references["
	  << lockable->references
	  << "] *pointer()["
	  << *pointer()
	  << "]"
	  << std::endl ;
      }
    }

    void swap( lockable_pointer_t & rhs )
    {
      std::swap( lockable, rhs.lockable ) ;
    }

    void reset()
    {
      lockable = 0 ;
    }

    void destroy()
    {
      if ( lockable )
      {
	if ( ! references() )
	{
	  type_t* type = reinterpret_cast<type_t*>( lockable->address ) ;
	  std::cerr
	    << __FILE__
	    << ":"
	    << __LINE__
	    << ":about to destroy lockable->references["
	    << lockable->references
	    << "] *type["
	    << *type
	    << "]"
	    << std::endl ;
	  if ( type )
	  {
	    delete type ;
	  }
	}
	delete lockable ;
	reset() ;
      }
    }
    const lockable_pointer_t& operator=( lockable_pointer_t& rhs )
    {
      lockable = rhs.lockable ;
      return *this ;
    }
    void inc()
    {
      if ( ! lockable ) throw null_lockable_t( __FILE__ ) ;
      lockable->inc() ;
    }
    void dec()
    {
      if ( ! lockable ) throw null_lockable_t( __FILE__ ) ;
      lockable->dec() ;
    }
    void lock()
    {
      if ( ! lockable ) throw null_lockable_t( __FILE__ ) ;
      lockable->lock() ;
    }
    void unlock()
    {
      if ( ! lockable ) throw null_lockable_t( __FILE__ ) ;
      lockable->unlock() ;
    }
    type_t* operator->()
    {
      if ( ! lockable ) throw null_lockable_t( __FILE__ ) ;
      return reinterpret_cast<type_t*>( lockable->address ) ;
    }
    type_t& operator* ()
    {
      if ( ! lockable ) throw null_lockable_t( __FILE__ ) ;
      return *reinterpret_cast<type_t*>( lockable->address ) ;
    }
    type_t* pointer   ()
    {
      if ( ! lockable ) throw null_lockable_t( __FILE__ ) ;
      return reinterpret_cast<type_t*>( lockable->address ) ;
    }
    type_t& operator[]( uint32_t i )
    {
      if ( ! lockable ) throw null_lockable_t( __FILE__ ) ;
      return reinterpret_cast<type_t*>( lockable->address )[i] ;
    }

    bool operator!() const
    {
      return lockable == 0 || lockable->address == 0 ;
    }
    bool operator()()const
    {
      return lockable != 0 && lockable->address != 0 ;
    }
    operator bool()  const
    {
      return lockable != 0 && lockable->address != 0 ;
    }
    bool malloced()  const
    {
      if ( ! lockable ) throw null_lockable_t( __FILE__ ) ;
      return lockable->malloced() ;
    }
    void mark_malloced()
    {
      if ( ! lockable ) throw null_lockable_t( __FILE__ ) ;
      lockable->malloc() ;
    }
    bool locked()  const
    {
      if ( ! lockable ) throw null_lockable_t( __FILE__ ) ;
      return lockable->locked() ;
    }
    uint64_t references()
    {
      return lockable ? lockable->references : 0 ;
    }
  } ;
}
#endif
