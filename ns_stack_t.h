/*
    ns_stack_t.h : example source for non-stop wait-free/weight-less
      [ low fat ] locking using extra bits in 64bit pointer
      
    Copyright (C) 2012 David Walter<walter.david@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

 */
#ifndef NS_STACK_H
#define NS_STACK_H
#include <algorithm>
#include <stdexcept>
#include <cstring>
#include <type_traits>
#define FULL_MEMORY_BARRIER __asm__ __volatile__("" : : : "memory" )
// #define FULL_MEMORY_BARRIER __sync_synchronize()
#include <ns_lockable_wrapper/ns_lockable_t.h>
#include <ns_lockable_wrapper/ns_lockable_pointer_t.h>
#include <ns_lockable_wrapper/ns_lockable_wrapper_t.h>
#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)

namespace ns
{
#ifndef NONSTOP_LOCKS_H__
  namespace
  {
    struct lockable_t
    {
      lockable_t( void* rhs )
      {
	memset( this, 0, sizeof( lockable_t ) ) ;
	object = rhs ;
      }

      lockable_t( lockable_t& rhs )
      {
	memcpy( this, &rhs, sizeof( lockable_t ) ) ;
      }

      lockable_t()
      {
	memset( this, 0, sizeof( *this ) ) ;
      }

#if __WORDSIZE == 64
      union
      {
	struct
	{
	  uint64_t address    : 52 ;
	  uint64_t unused     : 12 ;
	} ;
	struct
	{
	  uint32_t low_address_bits ;
	  union
	  {
	    uint32_t high_order_address ;
	    struct
	    {
	      uint64_t ignore_bits: 20 ;
	      uint64_t references :  9 ;
	      uint64_t reserved   :  1 ;
	      uint64_t malloc     :  1 ;
	      uint64_t locked     :  1 ;
	    } ;
	  } ;
	} ;
	void    *object ;
	uint64_t text ;
      } ;
#else
      struct
      {
	union
	{
	  void* object ;
	  void* address ;
	} ;
	union
	{
	  struct
	  {
	    uint32_t references : 29 ;
	    uint32_t reserved   :  1 ;
	    uint32_t malloc     :  1 ;
	    uint32_t locked     :  1 ;
	  } ;
	  uint32_t lock_bits ;
	} ;
      } ;
#endif

      bool operator!() const  { return address == 0 ; }
      bool operator()()const  { return address != 0 ; }
      operator bool()  const  { return address != 0 ; }
      bool malloced()  const  { return malloc ;       }
      void mark_malloced()    { malloc = 1 ;          }

      inline void lock()
      {
	bool done = false ;
	while( ! done )
	{
	  lockable_t lock( *this ) ;
	  lock.references = 0 ;
	  lock.locked = 0 ;
	  lockable_t current( lock ) ;
	  lock.locked = 1 ;
#if __WORDSIZE == 64
	  done =
	    __sync_bool_compare_and_swap( &high_order_address
					,  current.high_order_address
					,  lock.high_order_address ) ;
#else
	  done =
	    __sync_bool_compare_and_swap( &this->lock_bits
					,  current.lock_bits
					,  lock.lock_bits ) ;
#endif	  
	}
      }

      inline void unlock()
      {
	FULL_MEMORY_BARRIER ;
	this->locked = 0 ;
      }

      inline void add( int32_t i )
      {
	bool done = false ;
	while( ! done )
	{
	  lockable_t lock( *this ) ;
	  lockable_t current( lock ) ;
	  lock.references += i ;
	  lock.locked = 0 ;
#if __WORDSIZE == 64
	  done =
	    __sync_bool_compare_and_swap( &high_order_address
					,  current.high_order_address
					,  lock.high_order_address ) ;
#else
	  done =
	    __sync_bool_compare_and_swap( &this->lock_bits
					,  current.lock_bits
					,  lock.lock_bits ) ;
#endif	  
	}
      }

      inline void inc()
      {
	add( 1 ) ;
      }
      inline void dec()
      {
	add( -1 ) ;
      }

    } ;

    template <typename type_t>
    struct ns_pointer_t:  lockable_t
    {
      ns_pointer_t() {}
      ~ns_pointer_t(){}
      ns_pointer_t( lockable_t& lockable ) : lockable_t( lockable ){}
      ns_pointer_t( type_t* lockable ) : lockable_t( lockable ){}
#if __WORDSIZE == 64
      const ns_pointer_t<type_t>& operator=( type_t* rhs )
      {
	address = reinterpret_cast<uint64_t>(rhs) ; return *this ;
      }
#else
      const ns_pointer_t<type_t>& operator=( type_t* rhs )
      {
	address = rhs ; return *this ;
      }
#endif
      type_t* operator->() { return reinterpret_cast<type_t*>( this->address ) ;  }
      type_t& operator* () { return *reinterpret_cast<type_t*>( this->address ) ; }
      type_t* pointer   () { return reinterpret_cast<type_t*>( this->address ) ; }
      type_t& operator[]( uint32_t i )  { return reinterpret_cast<type_t*>( this->address )[i] ; }
      static void * operator new   ( size_t size )  ;
      static void   operator delete( void * p ) ;
      bool locked()
      {
	return this->locked ;
      }
    } ;

    struct writelock_t
    {
      lockable_t & lockable ;

      writelock_t( lockable_t& rhs ) : lockable( rhs )
      {
	lockable.lock() ;
      }

      ~writelock_t()
      {
	lockable.unlock() ;
      }
    } ;
  }
#endif
  template < typename type_t, bool pod=std::is_pod<type_t>::value >
  struct ns_stack_t
  {
    ns_stack_t() :
      head(0)
    , tail(0)
    {
    }

    struct link_t
    {
      typedef ns::ns_pointer_t<link_t> link_ptr ;
      link_t( type_t* rhs ):
	value( rhs )
      , next( nullptr )
      {
      }
      type_t*     value ;
      link_t*     next ;
      ~link_t()
      {
	next = 0 ;
      }
    } ;

    typedef ns::ns_pointer_t<link_t> link_ptr ;
    link_ptr   head ;
    link_ptr   tail ;
    void push( type_t rhs )
    {
      if ( rhs )
      {
	writelock_t lock( head ) ;
	if ( unlikely( ! head ) )
	{
	  head = tail = new link_t( new type_t( rhs ) ) ;
	}
	else
	{
	  tail->next = new link_t( new type_t( rhs ) ) ;
	  tail       = tail->next ;
	}
      }
    }

    type_t* pop()
    {
      writelock_t lock( head ) ;
      link_ptr link( head ) ;
      type_t* value( 0 ) ;
      if ( likely( link ) )
      {
	head = head->next ;
	if ( link->value )
	{
	  value = link->value ;
	}
      }
      if ( ! head )
      {
	head = tail = 0 ;
      }
      return value ;
    }
  } ;

  template < typename T >
  struct ns_stack_t< ns::lockable_wrapper_t< T >, false >
  {
    typedef lockable_wrapper_t< T > wrapper_t ;

    ns_stack_t() :
      head(0)
    , tail(0)
    {
    }

    struct link_t
    {
      typedef ns::ns_pointer_t<link_t> link_ptr ;
      link_t( wrapper_t&& rhs ):
	value( std::move( rhs ) )
      , next( nullptr )
      {
      }
      wrapper_t   value ;
      link_t*     next ;
      ~link_t()
      {
	next = 0 ;
      }
    } ;

    typedef ns::ns_pointer_t<link_t> link_ptr ;
    link_ptr   head ;
    link_ptr   tail ;

    void push( T* rhs )
    {
      if ( rhs )
      {
	writelock_t lock( head ) ;
	wrapper_t wrapper( 0 ) ;

	if ( unlikely( ! head ) )
	{
	  head = tail = new link_t( rhs ) ;
	  wrapper = tail->value ;
	}
	else
	{
	  tail->next = new link_t( rhs ) ;
	  tail       = tail->next ;
	  wrapper = tail->value ;
	}
      }
    }

    void push( const T& rhs )
    {
      push( new T( rhs ) ) ;
    }

    wrapper_t&& pop()
    {
      writelock_t lock( head ) ;
      link_ptr link( head ) ;
      wrapper_t value( 0 ) ;
      if ( likely( link ) )
      {
	head = head->next ;
	if ( link->value )
	{
	  value.take( link->value ) ;
	}
      }
      if ( ! head )
      {
	head = tail = 0 ;
      }
      return std::move( value ) ;
    }
  } ;
}
#endif
