/*
    ns_lockable_pointer_t.cc : example source for non-stop wait-free/weight-less
    [ low fat ] locking using extra bits in 64bit pointer
      
    Copyright (C) 2012 David Walter<walter.david@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

 */

#include <signal.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ns_lockable_wrapper/ns_lockable_pointer_t.h>
#include <ns_lockable_wrapper/ns_stack_t.h>

ns::ns_stack_t< int > stack ;

typedef ns::lockable_pointer_t< int > lockable_data_t ;


void* start( void* arg )
{
  uint64_t tries( 10 ) ;
  while ( tries-- )
  {
    for ( int i = 0 ; i < 100 ; i++ )
    {
      try
      {
        stack.push( i ) ;
      }
      catch( std::exception& e )
      {
        std::cerr << e.what() << std::endl ;
      }
    }

    while( true )
    {
      try
      {
	ns::lockable_wrapper_t<int>&& x( stack.pop() );
	if ( ! x )
	{
	  break ;
	}
        if ( x )
	{
          std::cerr << *x << ' ' ;
	}
      }
      catch( std::exception& e )
      {
        std::cerr << e.what() << std::endl ;
      }
    }
    std::cerr << std::endl ;
  }
  pthread_exit(0) ;
}

void handler       ( int signal )
{
  std::cerr << "signal[" << sys_siglist[signal] << std::endl ;
}

void* handler_thread( void* )
{
  sigset_t set;
  sigfillset( &set ) ;
  struct sigaction action;
  action.sa_flags = SA_SIGINFO;
  action.sa_handler = handler ;
  sigaction(SIGALRM, &action, NULL);
  sigaddset( &set, SIGUSR1 ) ;
  sigaddset( &set, SIGUSR2 ) ;
  sigaddset( &set, SIGTERM ) ;
  sigaddset( &set, SIGQUIT ) ;
  sigaddset( &set, SIGHUP  ) ;
  sigaddset( &set, SIGFPE  ) ;
  sigaddset( &set, SIGILL  ) ;
  sigaddset( &set, SIGSEGV ) ;
  sigaddset( &set, SIGBUS  ) ;
  sigaddset( &set, SIGABRT ) ;
  sigaddset( &set, SIGTRAP ) ;
  sigaddset( &set, SIGSYS  ) ;
  sigaddset( &set, SIGINT ) ;
  sigaddset( &set, SIGPIPE ) ;

  sigaction( SIGALRM, &action, 0 );
  sigaction( SIGUSR1, &action, 0 ) ;
  sigaction( SIGUSR2, &action, 0 ) ;
  sigaction( SIGTERM, &action, 0 ) ;
  sigaction( SIGQUIT, &action, 0 ) ;
  sigaction( SIGHUP , &action, 0 ) ;
  sigaction( SIGFPE , &action, 0 ) ;
  sigaction( SIGILL , &action, 0 ) ;
  sigaction( SIGSEGV, &action, 0 ) ;
  sigaction( SIGBUS , &action, 0 ) ;
  sigaction( SIGABRT, &action, 0 ) ;
  sigaction( SIGTRAP, &action, 0 ) ;
  sigaction( SIGSYS , &action, 0 ) ;
  sigaction( SIGINT , &action, 0 ) ;
  sigaction( SIGPIPE, &action, 0 ) ;

  pthread_sigmask(SIG_UNBLOCK, &set, NULL);

  while( true )
  {
    timespec ts = {1,0} ;
    nanosleep( & ts, 0 ) ;
  }
  return 0 ;
}

namespace ns
{
  struct TX
  {
    TX()
    {
      std::cerr << __FILE__ << std::endl ;
    }
    ~TX()
    {
      std::cerr << __FILE__ << std::endl ;
    }
  } ;

  template < typename data_t >
  lockable_wrapper_t<data_t>&& foo()
  {
    lockable_data_t item( new int( 3 ) ) ;
    lockable_wrapper_t< data_t> wrapper( std::move( item ) ) ;
    return std::move( wrapper ) ;
  }

  template < typename data_t >
  lockable_wrapper_t<data_t> bar()
  {
    lockable_data_t item( new int( 3 ) ) ;
    lockable_wrapper_t< data_t> wrapper( std::move( item ) ) ;
    return wrapper ;
  }
}


int main( int argc, char** argv )
{
  ns::lockable_wrapper_t<int>&& wrapper( ns::foo<int>() ) ;
  std::cerr << "wrapper[" << wrapper.lockable.references() << "]" << std::endl ;
  ns::lockable_wrapper_t<int>&& wrapper2( ns::bar<int>() ) ;
  std::cerr << "wrapper2[" << wrapper2.lockable.references() << "]" << std::endl ;
  ns::lockable_wrapper_t<int> wrapper3( wrapper2 ) ;
  std::cerr << "wrapper3[" << wrapper3.lockable.references() << "]" << std::endl ;
  {
    ns::ns_stack_t< ns::lockable_wrapper_t<int> > stack ;
    stack.push( new int(1) ) ;
    ns::lockable_wrapper_t<int>&& wrapper( stack.pop() ) ;
    std::cerr << "wrapper[" << wrapper.lockable.references() << "]" << std::endl ;
    // ns::lockable_wrapper_t<int> wrapper2( wrapper ) ;
    // std::cerr << "wrapper2[" << wrapper2.lockable.references() << "]" << std::endl ;
  }
  {
    ns::ns_stack_t< ns::lockable_wrapper_t<int> > stack ;
    stack.push( new int(1) ) ;
    ns::lockable_wrapper_t<int>&& wrapper( stack.pop() ) ;
    std::cerr << "wrapper[" << wrapper.lockable.references() << "]" << std::endl ;
    lockable_data_t data( new int( 4 ) ) ;
    {
      ns::lockable_wrapper_t<int> wrapper[10] ;
      for ( uint32_t i = 0 ; i < sizeof( wrapper )/sizeof( wrapper[0] ); i++ )
      {
	wrapper[i] = data ;
      }
      for ( uint32_t i = 0 ; i < sizeof( wrapper )/sizeof( wrapper[0] ); i++ )
      {
	std::cerr
	  << "*wrapper[" << i << "]=[" << *wrapper[i]
	  << "]wrapper[" << i << "]lockable.references=["
	  << wrapper[i].lockable.references()
	  << "]" << std::endl ;
      }
    }
    
  }

  sigset_t set;
  sigfillset( &set ) ;
  pthread_sigmask( SIG_BLOCK, &set, 0 ) ;
  pthread_t signal_thread ;
  pthread_create( &signal_thread, 0, &handler_thread, 0 ) ;

  pthread_attr_t attr;
  size_t stacksize;

  pthread_attr_init(&attr);
  pthread_attr_getstacksize(&attr, &stacksize); 
  // printf("%u\n", stacksize);

  try
  {
    const int max_threads( getenv( "THREADS" ) ? atoi( getenv( "THREADS" ) ) : 2 ) ;
    pthread_t thread[max_threads] ;
    for ( int i=0; i < max_threads; i++ )
    {
      // stacksize = 4194304 ;
      // stacksize = 16777216 ;
      stacksize = 1 << 16 ;
      pthread_attr_setstacksize(&attr, stacksize); 
      pthread_create( &thread[i], &attr, &start, reinterpret_cast<void*>( & stack ) ) ;
    }
    for ( int i=0; i < max_threads; i++ )
    {
      pthread_join( thread[i], 0 ) ;
    }
    std::ofstream o( "done" ) ;
    o << "done" << std::endl ;
  }
  catch( ... )
  {
  }
  return 0 ;
}
