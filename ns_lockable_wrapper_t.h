/*
    ns_lockable_wrapper_t.h :
    Example:  source for  non-stop  wait-free / weight-less [low fat]
    locking using extra bits in 64bit pointer for reference counter or
    data container pointer
      
    Copyright (C) 2012 David Walter<walter.david@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

 */

#ifndef NS_LOCKABLE_WRAPPER_T_H
#define NS_LOCKABLE_WRAPPER_T_H
#include <ns_lockable_wrapper/ns_lockable_pointer_t.h>
#ifndef NS_LOCKABLE_T_H
#error Do not #include <ns_lockable_wrapper/ns_lockable_wrapper_t.h> without
#error #include <ns_lockable_wrapper/ns_lockable_pointer_t.h>
#endif
#include <stdexcept>

namespace ns
{
  template < typename type_t >
  struct lockable_wrapper_t
  {
    typedef lockable_pointer_t< type_t > lockable_type_t ;
    lockable_type_t lockable ;
    bool locked ;

    lockable_wrapper_t( type_t* rhs=nullptr ):
      lockable( rhs )
    , locked( lockable ? true : false )
    {
      if ( lockable )
      {
	lockable.inc() ;
	std::cerr
	  << __FILE__
	  << ":"
	  << __LINE__
	  << ":lockable->references["
	  << lockable.lockable->references
	  << "] *lockable["
	  << *lockable
	  << "]"
	  << std::endl ;
      }
    }

    lockable_wrapper_t( lockable_type_t&& rhs ):
      lockable( rhs.lockable )
    , locked( rhs.lockable ? true : false )
    {
      if ( lockable )
      {
        lockable.inc() ;
	std::cerr
	  << __FILE__
	  << ":"
	  << __LINE__
	  << ":lockable.references()["
	  << lockable.references()
	  << "] *lockable["
	  << *lockable
	  << "]"
	  << std::endl ;
      }
    }

    void take( lockable_wrapper_t& rhs )
    {
      lockable = rhs.lockable ;
      locked   = rhs.locked   ;
      rhs.lockable.lockable = nullptr ;
      rhs.locked = false ;
    }

    lockable_wrapper_t( lockable_wrapper_t&& rhs ):
      lockable( rhs.lockable )
    , locked( rhs.locked ) 
    {
      rhs.locked = false ;
    }

    lockable_wrapper_t( lockable_wrapper_t& rhs ):
      lockable( rhs.lockable.pointer() )
    , locked( false ) 
    {
      if ( lockable )
      {
        lockable.inc() ;
        locked = true ;
      }
    }

    const lockable_wrapper_t& operator=( lockable_wrapper_t&& rhs )
    {
      lockable = rhs.lockable ;
      locked   = rhs.locked   ;
      rhs.locked = false ;
      return *this ;
    }

    const lockable_wrapper_t& operator=( lockable_wrapper_t& rhs )
    {
      lockable = rhs.lockable ;
      if ( lockable )
      {
        locked   = true ;
        lockable.inc() ;
      }
      return *this ;
    }

    lockable_wrapper_t& operator=( lockable_type_t& rhs )
    {
      lockable.lockable = rhs.lockable ;
      if ( lockable )
      {
        locked   = true ;
        lockable.inc() ;
      }
      return *this ;
    }

    ~lockable_wrapper_t()
    {
      if ( lockable && locked )
      {
        lockable.dec() ;
	if ( lockable.references() == 0 )
	{
	  std::cerr
	    << __FILE__
	    << ":"
	    << __LINE__
	    << ":lockable.references()["
	    << lockable.references()
	    << "]"
	    << std::endl ;
	  lockable.lock() ;
	  lockable.destroy() ;
	}
      }
    }
    uint32_t references()
    {
      return
	lockable && lockable.lockable ?
	lockable.lockable->references : 0 ;
    }
    operator bool()           { return lockable ; }
    bool operator !()         { return ! lockable ; }
    type_t& operator*()       { return *lockable.pointer() ; }
    type_t* operator->()      { return lockable.pointer() ; }
  } ;
}
#endif
