## Test make process
#VERSION=$(shell if [ -e version ]; then head -1 version; else echo 0.0.1; fi)
-include version
## Define the make sequence
.PHONY : clean .obj .lib .dep libs objs dynobjs bin
.PHONY : Build_Objects_Change Version_Change Build_Target_Change 
.PHONY : Lib_Change Dyn_Lib_Change Lib_Files_Change
.PHONY : install-lib install-bin install install-header
.PHONY : uninstall-lib uninstall-bin uninstall uninstall-header

## Default build type
DYNAMIC_BUILD=Y
OS:=$(shell uname|tr [:lower:] [:upper:])
BUILD_SYSTEM=$(shell uname -m)
DEVELOPMENT=$(HOME)/$(BUILD_SYSTEM)
LOCAL_LIB_DIR=.lib-$(BUILD_SYSTEM)
LOCAL_BIN_DIR=.bin-$(BUILD_SYSTEM)
LOCAL_DEP_DIR=.dep-$(BUILD_SYSTEM)
LOCAL_OBJ_DIR=.obj-$(BUILD_SYSTEM)
X=$(shell echo $${BUILD_SYSTEM} $${DEVELOPMENT} )
-include Makefile.os

ifeq ($(DEVELOPMENT),)
## Must define environment variable
FATAL: NO_DEVELOPMENT_PATH_DEFINED_MUST_DEFINE_$$DEVELOPMENT
	echo  NO_DEVELOPMENT_PATH_DEFINED_MUST_DEFINE_$$DEVELOPMENT
	exit
NO_DEVELOPMENT_PATH_DEFINED_MUST_DEFINE_$$DEVELOPMENT:
	echo You must define DEVELOPMENT=$$HOME or the like
	echo for the development and installation path
	exit 1
endif


all:
CXX_FILES=*.cc *.cpp *.cxx
IDL_FILES=*.idl
HEADERS_FILES=*.h *.tcc
HEADERS := $(wildcard *.h *.tcc)
INSTALL_HEADERS:=$(HEADERS)

CXXSOURCE =`pwd`
CXXHEADERS=. $(DEVELOPMENT)/include $(DEVELOPMENT)/include/libmemcached
LIBPATHS  =$(DEVELOPMENT)/lib
LIBS      = rt
-include Makefile.libs
CXXLIBPATHS=$(foreach path,$(LIBPATHS), -L$(path))
CXXLIBS    =$(foreach lib,$(LIBS), -l$(lib))
CXXINCLUDES=$(foreach dir, $(CXXHEADERS), -I$(dir))
#-include Makefile.os

HAVE_IDL:= $(shell if ls -1 $(wildcard $(IDL_FILES)) | head -1 > /dev/null 2>&1 ; then echo 1; else echo 0; fi)
HAVE_CXX:= $(shell if ls -1 $(wildcard $(CXX_FILES)) | head -1 > /dev/null 2>&1 ; then echo 1; else echo 0; fi)
HAVE_HEADERS:= $(shell if ls -1 $(HEADERS) | head -1 > /dev/null 2>&1 ; then echo 1; else echo 0; fi)

#CXX_BUILD:= $(shell if (( $(HAVE_CXX) )); then echo CXX; fi ) 
#IDL_BUILD:= $(shell if (( $(HAVE_IDL) )); then echo IDL; fi)
#HEADERS_BUILD:= $(shell if (( $(HAVE_HEADERS) )); then echo HEADERS; fi)

ifeq ($(HAVE_IDL),1)
INCLUDES += -Iidl
BUILDS  += $(IDL_BUILD)
endif

ifeq ($(HAVE_CXX),1)
INCLUDES += -I.
BUILDS  += $(CXX_BUILD)
endif

########################################################################
# Default installation directory is the home directory
########################################################################
LIBDIRS       := -L$(LIBINSTALLDIR)
########################################################################
# Default target is the directory's name file
########################################################################
BUILD     :=$(shell basename $(PWD))
ifneq ($(cleanOrInstall),Y)
INITIALIZE:=$(shell if [ ! -e $(BUILD) -a $(BUILD) != "test" ]; then ln -sf . $(BUILD); fi )
endif
########################################################################
# Default target object is where main lives int main(...)
########################################################################
greparg='main[ ]*\('
#SOURCEPAT='*.cc *.cpp *.cxx'
CXX_SOURCE_LIST:=$(wildcard *.cc *.cxx *.cpp)
BUILD_OBJECTS := $(shell						\
for name in $(CXX_SOURCE_LIST);						\
do									\
    if egrep $(greparg) $${name} >/dev/null 2>&1; then			\
		echo $(LOCAL_OBJ_DIR)/$${name} | /bin/sed -e "s,\.cc,\.o,g";	\
    fi									\
done)

TARGETS := $(shell						\
for name in $(CXX_SOURCE_LIST);					\
do								\
    if egrep $(greparg) $${name} >/dev/null 2>&1; then		\
		echo $(LOCAL_BIN_DIR)/$${name} | /bin/sed -e "s,\.cc,,g";	\
    fi								\
done)

FILTER_SOURCES_NAMES=								\
		`for name in  *.cc; do						\
			if egrep $(greparg) $${name}>/dev/null 2>&1; then	\
				echo -en "$${name} ";				\
			fi;							\
		done`


# BUILD_OBJECTS :=$(LOCAL_OBJ_DIR)/$(shell basename $(PWD)).o

#do								\
#    if grep $(greparg) $${name} >/dev/null 2>&1; then		\
#		echo $${name} | /bin/sed -e "s,\.cc,.o,g";	\
#    fi								\
#done)

########################################################################
# Default libfiles are all but the directory's name.{cc,cpp,cxx} file
# Default libfiles or rather excluding the target file
LIBFILES   :=$(filter-out $(BUILD)%,$(wildcard *.cc))
########################################################################
# or the following filter rule will remove any files which have a main
# ** Use with care as this may not work in the case of a file where the
# ** word main occurs in a line with an open paren main .... (
########################################################################
LIBFILES=$(shell						\
for name in $(CXX_SOURCE_LIST);					\
do								\
	if ! egrep $(greparg) $${name} >/dev/null 2>&1; then	\
		echo $${name};					\
	fi							\
done)




########################################################################
## Default lib name is the name of the current directory
########################################################################
#OPTIMIZATION        := -O0
# PROFILING           := -pg
DEBUGGING           := -g

LIBNAME    		 	:= $(shell basename $(PWD))
DYNLIBNAME 		 	:= $(shell basename $(PWD))

HEADERINSTALLDIR 	:= $(DEVELOPMENT)/include/$(LIBNAME)
LIBINSTALLDIR 	 	:= $(DEVELOPMENT)/lib
INSTALLDIR    	 	:= $(DEVELOPMENT)/bin

LINKLIBS    		+= $(LIBNAME)
DYNLINKLIBS 		+= $(DYNLIBNAME)

LIB_FILE_NAME    	:= lib$(shell basename $(PWD)).a
DYNLIB_FILE_NAME 	:= lib$(shell basename $(PWD)).so

HOLDBUILD        	:= $(BUILD)
HOLDLIB_FILE_NAME	:= $(LIB_FILE_NAME)
HOLDDYNLIB_FILE_NAME:= $(DYNLIB_FILE_NAME)
HOLDLIBFILES        := $(LIBFILES)
HOLDBUILDOBJECTS    := $(BUILD_OBJECTS)
HOLDVERSION         := $(VERSION)
#GXXVERSION          := $(shell gcc --version | egrep '.* ([0-9]*\.[0-9]*\.[0-9]*)' | sed -e 's/.* \([0-9]*\.[0-9]*\.[0-9]*\) *.*$/\1/g')
#GCCVER := $(shell gcc -dumpversion | cut -d. -f1,2)

#CXX      			:= g++ -std=gnu++0x
CXX      			:= g++
INCLUDES 			+= $(CXXINCLUDES) -I$(HEADERINSTALLDIR)
CXXFLAGS 			:= $(INCLUDES) $(CFLAGS) -URC_INVOKED  -Wall -std=gnu++11

-include Makefile.os

# Append options to those specific to the build environment.
CXXFLAGS            += $(OPTIMIZATION) $(DEBUGGING)
LINKARGS            += $(OPTIMIZATION) $(DEBUGGING) $(PROFILING)

ifneq ($(HOLDVERSION),$(VERSION))
ALL := Version_Change $(ALL)
endif
ifneq ($(HOLDBUILDOBJECTS),$(BUILD_OBJECTS))
ALL := Build_Objects_Change $(ALL)
endif

########################################################################
## Set the library version and version information for the file byNAM
## setting VERSION
########################################################################
ifneq ($(VERSION),)
LIB_FILE_NAME    :=lib$(shell basename $(PWD)).a.$(VERSION)
DYNLIB_FILE_NAME :=lib$(shell basename $(PWD)).so.$(VERSION)
endif


ifneq ($(HOLDDYNLIB_FILE_NAME),$(DYNLIB_FILE_NAME))
ALL := Dyn_Lib_Change $(ALL)
endif
ifneq ($(HOLDLIB_FILE_NAME),$(LIB_FILE_NAME))
ALL := Lib_Change $(ALL)
endif
ifneq ($(HOLDLIBFILES),$(LIBFILES))
ALL := Lib_Files_Change $(ALL)
endif
ifneq ($(HOLDBUILD),$(BUILD))
ALL := Build_Target_Change $(ALL)
endif

# ifneq ($(BUILD),)
# DEPS += $(shell									\
# for name in $(BUILD_OBJECTS); do						\
# 	cfile=$(patsubst %.cc,$(LOCAL_OBJ_DIR)/%.o,$${name});				\
# 	depfile=`echo $${name}|sed -e "s,$(LOCAL_OBJ_DIR)/\([^\.]*\)\.o,$(LOCAL_DEP_DIR)/\1.d,"`;	\
#     if [ -e $${cfile} ]; then							\
# 		echo $${depfile};						\
# 	fi;									\
# done )
# endif

ifneq ($(LIBFILES),)
DEPS += $(shell								\
for name in $(LIBFILES); do						\
	depfile=`echo $${name}|sed -e "s,\([^\.]*\)\.cc,$(LOCAL_DEP_DIR)/\1.d,"`;	\
	if [ -e $${name} ]; then					\
		echo $${depfile};					\
	fi;								\
done )
endif

#-include Makefile.libs
#-include Makefile.definitions
#-include Makefile.targets

TEMP=$(BUILD) $(LIBFILES)
ifneq ($(TEMP),)
ALL +=$(LOCAL_DEP_DIR) $(LOCAL_OBJ_DIR)
endif

ifneq ($(LIBFILES),)
ALL +=$(LOCAL_LIB_DIR) $(LOCAL_LIB_DIR)/$(LIB_FILE_NAME) $(LOCAL_LIB_DIR)/$(DYNLIB_FILE_NAME) 
endif

ifneq ($(BUILD),)
ALL +=$(LOCAL_BIN_DIR) $(TARGETS)
endif

DEPEND=$(shell if [ -e $(LOCAL_DEP_DIR) ] && ls -1 $(LOCAL_DEP_DIR)/*.d >/dev/null 2>&1; then echo 1; fi)
TARTEMP    	   :=$(strip $(findstring tar,$(MAKECMDGOALS)))
haveTar    	   :=$(TARTEMP:tar=Y)
INSTALLTEMP    :=$(strip $(findstring install,$(MAKECMDGOALS)))
haveInstall    :=$(INSTALLTEMP:install=Y)
CLEANTEMP      :=$(strip $(findstring clean,$(MAKECMDGOALS)))
haveClean      :=$(CLEANTEMP:clean=Y)
cleanOrInstall := $(findstring Y,$(haveClean) $(haveInstall) $(haveTar))

########################################################################
## ifneq TESTDIR
########################################################################
TESTDIR        :=$(shell basename $(PWD))
ifneq ($(TESTDIR),test) 
ifneq ($(ALL),)
all: $(DEPS) $(ALL) $(LISTMAIN)
	@echo "ALL : $(ALL)"
	@echo "DEPS: $(DEPS)"
else
all: $(LOCAL_DEP_DIR) $(ALL) $(LISTMAIN)
	@echo built $(BUILD) $(LIBFILES) 
endif
else  ## ifeq TESTDIR

## A Test Directory 
TARGET1:=$(patsubst %.cc,%,$(wildcard *.cc))
TARGET2:=$(patsubst %.cxx,%,$(wildcard *.cxx))
TARGET3:=$(patsubst %.cpp,%,$(wildcard *.cpp))
TARGETS:=$(TARGET1) $(TARGET2) $(TARGET3)

ifneq ($(cleanOrInstall),Y)
%: %.cc $(wildcard Makefile*)
	$(CXX) -o $@ $@.cc $(LINKARGS) -I.. $(CXXINCLUDES) $(CXXLIBPATHS) $(CXXLIBS) $(DYNLINKARGS) ;
ALL    :=$(TARGETS)
all: $(ALL)
else
all: 
	echo "Don't make test stubs for clean."
endif
endif ## ifneq TESTDIR
########################################################################
## ifneq TESTDIR
########################################################################

########################################################################
### don't create subdirectories for test subdirectory.
########################################################################
ifneq ($(TESTDIR),test)  ## Create for non test stub

ifneq ($(cleanOrInstall),Y) # don't remake dependencies for those in this list.
ifeq ($(DEPEND),1)
-include $(patsubst %.cc,$(LOCAL_DEP_DIR)/%.d,$(wildcard $(CXX_FILES)))
endif
endif

$(LOCAL_DEP_DIR):
	@if [ ! -e $@ ]; then						\
        mkdir -p $@;							\
    fi

$(LOCAL_OBJ_DIR):
	@if [ ! -e %@ ]; then						\
        mkdir -p $@;							\
    fi

$(LOCAL_LIB_DIR):
	@if [ ! -e $@ ]; then						 \
        mkdir -p $@;							 \
    fi;											

#		touch -d "`date --date='Jan 1 12:01:01 2000'`" $(patsubst %.$(VERSION),%,$(LOCAL_LIB_DIR)/$(LIB_FILE_NAME));
#		touch -d "`date --date='Jan 1 12:01:01 2000'`" $(patsubst %.$(VERSION),%,$(LOCAL_LIB_DIR)/$(DYNLIB_FILE_NAME)); \

$(LOCAL_BIN_DIR):
	@if [ ! -e $@ ]; then						\
        mkdir -p $@;							\
    fi
endif ## Create for non test stub

ifneq ($(LIBFILES),)
#DEPS +=$(shell for lib in $(LIBFILES); do file=`echo $${lib}|sed -e "s,\.cc,,g"`; echo $(LOCAL_DEP_DIR)/$${file}.cc; done)
DYNLIBOBJS :=$(patsubst %.cc,$(LOCAL_OBJ_DIR)/%.so,$(LIBFILES))
#$(shell for lib in $(LIBFILES); do file=`echo $${lib}|sed -e "s,\.cc,,g"`; echo $(LOCAL_OBJ_DIR)/$${file}.so; done)
LIBOBJS    :=$(patsubst %.cc,$(LOCAL_OBJ_DIR)/%.o,$(LIBFILES))
#$(shell for lib in $(LIBFILES); do file=`echo $${lib}|sed -e "s,\.cc,,g"`; echo $(LOCAL_OBJ_DIR)/$${file}.o; done)

libobjs:: $(LIBOBJS)     $(LIBFILES)
dynobjs:: $(DYNLIBOBJS)  $(LIBFILES)

libs:: $(LOCAL_LIB_DIR) $(libobjs) $(dynobjs) $(LOCAL_LIB_DIR)/$(LIB_FILE_NAME) $(LOCAL_LIB_DIR)/$(DYNLIB_FILE_NAME) 

ifneq ($(VERSION),)
LINKTOLIB_FILE_NAME    :=lib$(shell basename $(PWD)).a
endif
$(LOCAL_LIB_DIR)/$(LIB_FILE_NAME):: $(LIBOBJS) $(LIBFILES)
	@$(AR) -ruv $@ $(filter-out %.cc Makefile%,$^)
#> /dev/null
ifneq ($(VERSION),)
	ln -sf $(LIB_FILE_NAME) $(LOCAL_LIB_DIR)/$(LINKTOLIB_FILE_NAME) 
endif

ifneq ($(VERSION),)
DYNLINKTOLIB_FILE_NAME :=lib$(shell basename $(PWD)).so
endif
$(LOCAL_LIB_DIR)/$(DYNLIB_FILE_NAME):: $(DYNLIBOBJS) $(LIBFILES)
	@$(AR) -ruv $@ $(filter-out %.cc Makefile%,$^)
#	@$(AR) -ruv $@ $^ 
#> /dev/null
ifneq ($(VERSION),)
	ln -sf $(DYNLIB_FILE_NAME) $(LOCAL_LIB_DIR)/$(DYNLINKTOLIB_FILE_NAME) 
endif

#$(LOCAL_LIB_DIR)/$(LIB_FILE_NAME)   :: $(LIBOBJS)     $(wildcard Makefile*)
#$(LOCAL_LIB_DIR)/$(DYNLIB_FILE_NAME):: $(DYNLIBOBJS)  $(wildcard Makefile*)

endif # LIBFILES

ifeq (A,B)
LINKLIBLIST +=															\
$(shell																	\
	if [ ! -z "$(LINKLIBS)" ]; then										\
	   for lib in $(LINKLIBS); do										\
		   name="`echo $${lib}|sed -e "s,$(LOCAL_LIB_DIR)/lib\([^\.]*\)\.a,\1,g"`";	\
		   liblist="$${liblist} -l$${name}";							\
	   done;															\
	   if [ ! -z "$${liblist}" ]; then									\
		   echo $${liblist};											\
	   fi																\
	fi																	\
)

DYNLINKLIBLIST +=															\
$(shell																		\
	if [ ! -z "$(DYNLINKLIBS)" ]; then										\
	   for lib in $(DYNLINKLIBS); do										\
		   name="`echo $${lib}|sed -e "s,$(LOCAL_LIB_DIR)/lib\([^\.]*\)\.so,\1,g"`";	\
		   liblist="$${liblist} -l$${name}";								\
	   done;																\
	   if [ ! -z "$${liblist}" ]; then										\
		   echo $${liblist};												\
	   fi																	\
	fi																		\
)

LIBS +=																	\
$(shell bash -c															\
'      for lib in $(wildcard $(LOCAL_LIB_DIR)/lib*.a); do							\
		   name="`echo $${lib}|sed -e "s,$(LOCAL_LIB_DIR)/lib\([^\.]*\)\.a,\1,g"`";	\
		   liblist="$${liblist} -l$${name}";							\
	   done;															\
	   echo $(strip $${liblist});										\
')

DYNLIBS +=																\
$(shell bash  -c														\
'   for lib in $(wildcard $(LOCAL_LIB_DIR)/lib*.so); do								\
		name="`echo $${lib}|sed -e "s,$(LOCAL_LIB_DIR)/lib\([^\.]*\)\.so,\1,g"`";	\
        liblist="$${liblist} -l$${name}";								\
    done;																\
	echo $(strip $${liblist});											\
')

ifneq ($(DYNLIBS),)
	DYNLIBDIR += -L$(LOCAL_LIB_DIR)
endif
ifneq ($(LIBS),)
	LIBDIR += -L$(LOCAL_LIB_DIR)
endif

LINKARGS +=$(LIBDIR) 
LINKARGS +=$(LIBDIRS) 
LINKARGS +=$(LIBS) 
LINKARGS +=$(LINKLIBLIST) 

DYNLINKARGS +=$(DYNLIBDIR) 
DYNLINKARGS +=$(LIBDIRS) 
DYNLINKARGS +=$(DYNLIBS) 
DYNLINKARGS +=$(DYNLINKLIBLIST) 

endif

ifneq ($(BUILD_OBJECTS),)
#$(LOCAL_LIB_DIR)/$(LIB_FILE_NAME)   :: $(LIBOBJS)     $(wildcard Makefile*)
#$(LOCAL_LIB_DIR)/$(DYNLIB_FILE_NAME):: $(DYNLIBOBJS)  $(wildcard Makefile*)
ifeq (DYNAMIC_BUILD),Y)
BUILD_LIBRARIES :=$(LOCAL_LIB_DIR)/$(DYNLIB_FILE_NAME)
DYNLINKARGS     = -shared-libgcc -shared -fPIC
else
BUILD_LIBRARIES :=$(LOCAL_LIB_DIR)/$(LIB_FILE_NAME)
endif
#-include Makefile$(LOCAL_LIB_DIR)raries
LINKARGS    +=$(shell if [ -e $(LIBINSTALLDIR)/$(LIB_FILE_NAME) ]; then echo "-L$(LIBINSTALLDIR) -l$(LIBNAME)"; fi)
LINKARGS    +=$(shell if [ -e $(BUILD_LIBRARIES) ]; then echo "-L$(LOCAL_LIB_DIR) -l$(LIBNAME)"; fi)

$(LOCAL_DEP_DIR)/%.d : %.cc $(wildcard Makefile*)
	@if [ ! -e $(LOCAL_DEP_DIR) ]; then mkdir -p $(LOCAL_DEP_DIR); fi
	@echo build dependency list for $@
	@$(CXX) -MM $(CXXFLAGS) $(CPPFLAGS) $<		\
		| sed -e "s,$*\\.o[ :]*,& $(LOCAL_OBJ_DIR)/$*.o:,g"	\
	          -e "s,$*\\.o[ :]*,$(LOCAL_OBJ_DIR)/$*.o: ,g"	\
	          -e "s,\\$(LOCAL_OBJ_DIR)/$(LOCAL_OBJ_DIR)/$*.o[^:]*:,,g" > $@

$(LOCAL_OBJ_DIR)/%.o : %.cc $(wildcard Makefile*)
	$(CXX) -c -o $@ $< $(CXXFLAGS) 

$(LOCAL_OBJ_DIR)/%.so: %.cc 
	$(CXX) -c -o $@ $< $(CXXFLAGS) -shared -fPIC

$(LOCAL_BIN_DIR)/%    : $(LOCAL_OBJ_DIR)/%.o $(BUILD_LIBRARIES) $(wildcard Makefile*)
	$(CXX) -o $@ $(patsubst $(LOCAL_BIN_DIR)/%,$(LOCAL_OBJ_DIR)/%.o,$@) $(CXXLIBS) $(BUILD_LIBRARIES) $(LINKARGS) \
		    $(DYNLINKARGS) $(CXXLIBPATHS) ;

endif
########################################################################
## ifneq TESTDIR ( dir name == test )
########################################################################
ifneq ($(TESTDIR),test)  ## Create for non test stub
clean:
	rm -rf $(LOCAL_BIN_DIR) $(LOCAL_OBJ_DIR) $(LOCAL_LIB_DIR) $(LOCAL_DEP_DIR) $@
	if [ -L $(BUILD) -a $(BUILD) != "test" ]; then rm -f $(BUILD); fi
else
clean:
#	@echo "install:    	   " $(haveInstall)
#	@echo "clean:      	   " $(haveClean)
#	@echo "cleanOrInstall: " $(cleanOrInstall)
#	echo $(TARGETS)
	rm -rf $(TARGETS)
endif

status:
	@echo "Default Target list $^"
	@echo "Options"
	@echo "BUILD_OBJECTS $(BUILD_OBJECTS)"
	@echo "TARGETS     	 $(TARGETS)"
	@echo "CXXSOURCE   	 $(CXXSOURCE)"
	@echo "CXXHEADERS  	 $(CXXHEADERS)"
	@echo "LIBPATHS    	 $(LIBPATHS)"
	@echo "LIBS        	 $(LIBS)"
	@echo "CXXLIBPATHS 	 $(CXXLIBPATHS)"
	@echo "CXXLIBS     	 $(CXXLIBS)"
	@echo "CXXINCLUDES 	 $(CXXINCLUDES)"
	exit 1

########################################################################
# START: Report state changes from the defaults
########################################################################
Build_Objects_Change::
	@echo "Build Objects changed from $(HOLDBUILDOBJECTS) to $(BUILD_OBJECTS)"
Version_Change::
	@echo "Version changed from $(HOLDVERSION) to $(VERSION)"
Build_Target_Change::
	@echo "Target changed from $(HOLDBUILD) to $(BUILD)"
Lib_Change::
	@echo "Lib name changed from $(HOLDLIB_FILE_NAME) to $(LIB_FILE_NAME)"
Dyn_Lib_Change::
	@echo "Dynamic libs changed from $(HOLDDYNLIB_FILE_NAME) to $(DYNLIB_FILE_NAME)"
Lib_Files_Change::
	@echo "Lib files changed from $(HOLDLIBFILES) to $(LIBFILES)"
########################################################################
# END: Report state changes from the defaults
########################################################################

########################################################################
# Install targets
# START: ifneq ($(TESTDIR),test) 
########################################################################
ifneq ($(TESTDIR),test)
install: $(LOCAL_OBJ_DIR) $(LOCAL_LIB_DIR) $(LOCAL_DEP_DIR) $(LOCAL_BIN_DIR) install-lib install-bin install-header

install-lib :: $(LOCAL_LIB_DIR)/$(LIB_FILE_NAME) $(LOCAL_LIB_DIR)/$(DYNLIB_FILE_NAME)
	mkdir -p $(LIBINSTALLDIR)
	cp -rp $^ $(LIBINSTALLDIR)
ifneq ($(VERSION),)
	if [ -e $(LOCAL_LIB_DIR)/$(LIB_FILE_NAME) ]; then ln -sf $(LIB_FILE_NAME) $(LIBINSTALLDIR)/$(LINKTOLIB_FILE_NAME) ; fi
endif
ifneq ($(VERSION),)
	if [ -e $(LOCAL_LIB_DIR)/$(DYNLIB_FILE_NAME) ]; then ln -sf $(DYNLIB_FILE_NAME) $(LIBINSTALLDIR)/$(DYNLINKTOLIB_FILE_NAME) ; fi
endif

install-header :: $(HEADERS)
	mkdir -p $(HEADERINSTALLDIR)
	cp -rp $^ $(HEADERINSTALLDIR)

install-bin :: $(TARGETS)
	mkdir -p $(INSTALLDIR)
	cp -rp $^ $(INSTALLDIR)

uninstall: uninstall-lib uninstall-bin uninstall-header

uninstall-lib: 
	rm -f $(LIBINSTALLDIR)/$(LIB_FILE_NAME) $(LIBINSTALLDIR)/$(DYNLIB_FILE_NAME)
ifneq ($(VERSION),)
	rm -f $(LIBINSTALLDIR)/$(LINKTOLIB_FILE_NAME)
	rm -f $(LIBINSTALLDIR)/$(DYNLINKTOLIB_FILE_NAME)
endif

uninstall-bin: 
	for name in $(TARGETS); do rm -f $(INSTALLDIR)/$${name}; done

uninstall-header: $(HEADERS)
	for name in $(HEADERS); do rm -f $(HEADERINSTALLDIR)/$${name}; done

########################################################################
# Install targets
########################################################################
else  # eq ($(TESTDIR),test) 
install			\
install-lib		\
install-header	\
install-bin		\
uninstall		\
uninstall-bin	\
uninstall-lib	\
uninstall-header:
	@echo "Target( $@ ) has no action in the test directory ( $(PWD) )"
endif # ifneq ($(TESTDIR),test) 
########################################################################
# END: ifneq ($(TESTDIR),test) 
########################################################################

LISTMAIN:
	@echo "Filtered LIBFILES $(LIBFILES)"
	@echo "Files with main in them $(FILTER_SOURCES_NAMES)"
	@echo "Objects to be built which have main: $(BUILD_OBJECTS)"

TARFILE=$(LIBNAME)-$(VERSION)-$(shell date +%Y.%m.%d).tar.gz 
GITTARFILE=$(LIBNAME)-$(VERSION)-$(shell date +%Y.%m.%d)-git.tar.gz 

tar: version $(TARFILE)

version:
	echo VERSION=0.0.1 > version
dir=$(shell basename $(PWD))

tarfiles:=														\
	$(shell														\
		for name in												\
            $(wildcard *.cc *.cxx *.cpp *.tcc *.h Makefile*);	\
	    do														\
			echo $(dir)/$${name};								\
		done;													\
		for name in												\
            $(wildcard test/*.cc test/*.cxx						\
				test/*.cpp test/*.tcc test/*.h test/Makefile*);	\
	    do														\
			echo $(dir)/$${name};								\
		done | sort												\
	)
$(TARFILE): Makefile
	@( cd ..; tar czf $(dir)/$(TARFILE) $(tarfiles) )
	@( cd ..; tar czf $(dir)/$(GITTARFILE) $(dir)/.git $(tarfiles) )

# local variables:
# mode: makefile
# tab-width: 4
# end:
TEST: all
#	for USE_FUTEX in 0 1 ; do for threads in 10 100; do for command in ns-100 ns; do for var in USE_LOCK USE_MINUS_ONE NS_USE_PTHREAD_MUTEX USE_CAS; do unset USE_LOCK USE_MINUS_ONE NS_USE_PTHREAD_MUTEX USE_CAS USE_TEST_AND_SET; export THREADS=$${threads}; if [ "$${USE_FUTEX}" = "1" ]; then export USE_FUTEX=1; fi; export $${var}=1; env | grep USE_ ; echo USE_FUTEX=$${USE_FUTEX} THREADS=$${threads} $${command} ; time -p .bin-x86_64/$${command} 2>&1 | egrep -v USE\|^[0-9]\|MAX_R ; done; done; done; done

	for USE_FUTEX in 0 1 ; do for threads in 100; do for command in ns-100; do for var in USE_LOCK USE_MINUS_ONE NS_USE_PTHREAD_MUTEX USE_CAS; do unset USE_LOCK USE_MINUS_ONE NS_USE_PTHREAD_MUTEX USE_CAS USE_TEST_AND_SET; export THREADS=$${threads}; if [ "$${USE_FUTEX}" = "1" ]; then export USE_FUTEX=1; fi; export $${var}=1; echo $${var}=1 USE_FUTEX=$${USE_FUTEX} THREADS=$${threads} $${command} ; time -p .bin-x86_64/$${command} 2>&1 | egrep -v USE\|^[0-9]\|MAX_R ; done; done; done; unset USE_FUTEX ; done