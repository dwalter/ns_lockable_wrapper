ifeq ($(OS),LINUX)
CXXFLAGS+=					\
 -D__UNIXOS2__					\
 -I.						\
 -I$(DEVELOPMENT)/include			\

LINKARGS+= -O3
LIBPATHS+=$(DEVELOPMENT)/lib
LIBS+=pthread rt

endif # LINUX

# local variables:
# mode: makefile
# end:

